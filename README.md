# Carrot php


> Carrot is the wrapper over rabbitmq using the official rabbitmq php library
[php-amqplib/php-amqplib](https://github.com/php-amqplib/php-amqplib)

### Required extension
 - bcmath

```bash
# Docker install
RUN docker-php-ext-install bcmath
```

### Installation
```bash
# Require this package with composer:
composer require quince/carrot-php
```

### How to use

```php
use Quince\Carrot\Carrot;

$config = [
    'host' => 'localhost', 
    'port' => '5672', 
    'exchange' => 'ampq',
    'logging', => false
];

$carrot = new Carrot($config);
```
### Use in laravel/lumen with global configuration

- Add configuration [file](config/carrot.php) to `config/carrot.php` 
- Add new environment variables for carrot.
 

```bash
CARROT_HOST=localhost
CARROT_PORT=5672
CARROT_EXCHANGE=ampq
```

### Start using carrot in your code

```php
use Quince\Carrot\Carrot;

$carrot = new Carrot();
```

### Emit messages
```php
$carrot->emit($topic, $playload, $options);
# Options are not required
$options = [
    'uuid' => $uuid,
    'parent_uuid' => null,
    'correlation_id' => $uuid,
    'timestamp' => 'nanotime',
    'tenant_id' => null,
    'env' => 'production',
    'volatile' => true
];
```

### Listen messages
```php
$carrot->listen($queue, $topics, $handler);
```
#### Carrot also accept optional configuration
> By setting `logging` to true, all your events recevied in the handler will be emitted to Logging module

```bash
# In your php code
$config = [
    'logging' => false, 
    'localHost' => 'my-localhost', 
    'instance' => 'my-instance', 
    'module' => 'my-module', 
    'appVersion' => '1.2.3-dev'
];
```

```bash
# In your .env files
CARROT_LOGGING=false
CARROT_LOCALHOST=module.example.com
CARROT_MODULE=example.module
CARROT_ISTANCE=example.istane
CARROT_APP_VERSION=1.0
```
