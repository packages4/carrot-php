<?php

namespace Quince\Carrot;

use Exception;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Exception\AMQPRuntimeException;
use PhpAmqpLib\Wire\AMQPTable;
use Nanotime\Nanotime;
use Quince\Carrot\Correlation;
use Quince\Carrot\Lib\Uuid;

class Carrot
{
    const LOGGING_EXCHANGE = 'platform_logging';

    protected $port;
    protected $host;
    protected $connection;
    protected $exchange;
    protected $channel;
    protected $logging;

    protected $localHost;
    protected $instance;
    protected $module;
    protected $appVersion;

    /**
     * Default options used for queue_declare , exchange_declare and queue_consume.
    */
    protected $defaultOptions = [
        'recreate' => false,
        'passive' => false,
        'durable' => false,
        'exclusive' => false,
        'auto_delete' => true,
        'consumer_tag' => '',
        'no_local' => false,
        'no_ack' => true,
        'nowait' => false
    ];

    protected $queues = [];

    /**
     * @param string $host
     * @param mixed $port
     * @param bool $exchange
     * @param int $protocol
     */
    public function __construct($config = [])
    {
        if (empty($config)) {
            $config = config('carrot.config');
        }

        $this->host = $config['host'] ?? getenv('CARROT_HOST');
        $this->port = $config['port'] ?? getenv('CARROT_PORT');
        $this->exchange = $config['exchange'] ?? getenv('CARROT_EXCHANGE');
        $this->logging = $config['logging'] ?? getenv('CARROT_LOGGING') ?? false;

        $this->localHost = $config['localHost'] ?? getenv('QUINCE_HOSTNAME') ?? null;
        $this->instance = $config['instance'] ?? getenv('QUINCE_INSTANCE') ?? null;
        $this->module = $config['module'] ?? getenv('QUINCE_MODULE') ?? null;
        $this->appVersion = $config['appVersion'] ?? getenv('QUINCE_APP_VERSION') ?? null;

        $this->queues = [];

        $this->connect();
    }

    /**
     * Connect to a specified rabbitmq server,
     * Assert a channel
     * Declare topic exchange
     * @param array $passedOptions
     *
     * @return void
     */
    protected function connect($passedOptions = ['auto_delete' => false])
    {
        if ($this->connection) {
            return $this->connection;
        }

        $option = array_merge($this->defaultOptions, $passedOptions);
        try {
            $this->connection = $this->createConnection();
            $this->channel = $this->getChannel();
            $this->channel->exchange_declare(
                $this->exchange,
                'topic',
                $option['passive'],
                $option['durable'],
                $option['auto_delete']
            );

            if ($this->logging) {
                $this->channel->exchange_declare(
                    self::LOGGING_EXCHANGE,
                    'topic',
                    $option['passive'],
                    $option['durable'],
                    $option['auto_delete']
                );
            }

            $this->requeue();
        } catch (Exception $e) {
            \Log::error('Check your rabbitmq connection.');
        }
    }

    /**
     * Publishes a message
     *
     * Emit an event with a specified $topic and $payload
     *
     * @param string $topic
     * @param mixed $payload
     * @param array $passedOptions
     * @param bool $mandatory
     * @param bool $immediate
     * @param int $ticket
     *
     * @return void
     */
    public function emit($topic, $payload, $passedOptions = [], $mandatory = false, $immediate = false, $ticket = null)
    {
        $uuid = Uuid::v4();

        $defaultOptions = [
            'uuid' => $uuid,
            'parent_uuid' => null,
            'correlation_id' => Correlation::getCorrelationId(),
            'timestamp' => Nanotime::now()->nanotime(),
            'tenant_id' => null,
            'env' => 'PROD',
            'volatile' => true
        ];

        $mergedOptions = array_merge($defaultOptions, $passedOptions);

        if ($this->connect()) {
            $message = new AMQPMessage($payload);
            $headers = new AMQPTable([
                'parentId' => $mergedOptions['parent_uuid'],
                'timestamp' => $mergedOptions['timestamp'],
                'volatile' => $mergedOptions['volatile'],
                'env' => $mergedOptions['env']
            ]);

            $message->set('application_headers', $headers);
            $message->set('message_id', $mergedOptions['uuid']);
            $message->set('correlation_id', $mergedOptions['correlation_id']);
            $message->set('app_id', $mergedOptions['tenant_id']);

            $this->channel->basic_publish($message, $this->exchange, $topic, $mandatory, $immediate, $ticket);
        }
    }

    /**
     * Starts a queue consumer
     *
     * Listen on a named $queue to a set of $topics and consume events with a $handler
     *
     * @param string $queue
     * @param array $topics
     * @param callback|null $handler
     * @param bool $recreate
     *
     * @return void
    */
    public function listen($queue, $topics, $handler, $passedOptions = [])
    {
        $option = array_merge($this->defaultOptions, $passedOptions);

        if ($this->connect()) {
            // If we don't have a queue with this name yet (or want to recreate it), create it
            if (!isset($this->queues[$queue]) or $option['recreate']) {
                if ($this->channel->queue_declare(
                    $queue,
                    $option['passive'],
                    $option['durable'],
                    $option['exclusive'],
                    $option['auto_delete'])
                ) {
                    $this->queues[$queue]['topics'][] = $topics;
                    $this->queues[$queue]['handlers'][] = $handler;
                }
            }

            // Bind the requested topics to it and store their handler
            foreach ($topics as $topic) {
                $this->channel->queue_bind($queue, $this->exchange, $topic);
            }

            $handle = function ($message) use ($queue, $handler) {
                $this->handle($queue, $message, $handler);
            };

            // Start consuming messages for this queue
            $this->channel->basic_consume(
                $queue,
                $option['consumer_tag'],
                $option['no_local'],
                $option['no_ack'],
                $option['exclusive'],
                $option['nowait'],
                $handle
            );

            // This will strat never endless process
            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }
        }
    }

    /**
     * @param $queue
     * @param $event
     * @param $handler
     *
     * @return $closure
     */
    protected function handle($queue, $event, $handler)
    {
        if ($this->logging) {
            $result = null;
            $error = null;

            $timer['start'] = Nanotime::now();

            try {
                $result = $handler($event);
            } catch (\Exception $e) {
                $error = $e;
            }

            $duration = Nanotime::now()
                ->diff($timer['start'])
                ->nanotime();

            $this->logHandler($queue, $event, $duration, $error);

            return $result;
        }

        return $handler($event);
    }

    /**
     * @param $queue
     * @param $event
     * @param $duration
     * @param $error
     *
     * @return void
     */
    protected function logHandler($queue, $event, $duration, $error)
    {
        if (!is_null($error)) {
            $error = sha1((string) $error);
            $error = substr($error, 0, 8);
        }

        $entry = [
            'hostname' => $this->localHost,
            'instance' => $this->instance,
            'module' => $this->module,
            'event_handler' => $queue,
            'event_name' => $event->get('routing_key') ?? null,
            'app_version' => $this->appVersion,
            'total_duration' => $duration,
            'error_code' => $error,
            'created_at' => Nanotime::now()->nanotime()
        ];

        $options = [
            'uuid' => $event->get('message_id') ?? null,
            'correlation_id' => $event->get('correlation_id') ?? $event->get('message_id')
        ];

        $this->log('platform.log.event', $entry, $options);
    }

    /**
     * Emit log
     *
     * @param string $topic
     * @param mixed $payload
     *
     * @return void
     */
    protected function log($topic, $payload, $options)
    {
        if ($this->connect()) {
            $message = new AMQPMessage(json_encode($payload, true));

            $message->set('correlation_id', $options['correlation_id']);
            $message->set('message_id', $options['uuid']);

            $this->channel->basic_publish($message, self::LOGGING_EXCHANGE, $topic);
        }
    }

    /**
     * Recreate all existing queues and their handlers for their respective topics
     *
     * @return void
     */
    protected function requeue()
    {
        foreach ($this->queues as $queue) {
            foreach ($queue['handlers'] as $handler) {
                $this->listen($queue, $queue['topics'], $handler, ['recreate' => true]);
            }
        }
    }

    /**
     * Create new rabbitmq connection.
     *
     * @return RabbitMq connection
     */
    protected function createConnection()
    {
        // Create connection with AMQP
        $this->connection = new AMQPStreamConnection(
            $this->host,
            $this->port,
            'guest',
            'guest'
        );

        return $this->connection;
    }

    /**
     * @return RabbitMq channel
     */
    protected function getChannel()
    {
        return $this->connection->channel();
    }

    /**
     * Shoutdown connection
     *
     * @return void
     */
    protected function shoutdown()
    {
        $this->channel->close();
        $this->connection->close();
    }
}
