<?PHP
namespace Quince\Carrot;

use Quince\Carrot\Lib\Uuid;

class Correlation
{

    /**
     * After request is finished generate correlation id
     * @param string
     */
    protected static $correlationId;

    /**
     * Set the correlation ids
     * @param string $id
     * @return void
     */

    public static function setCorrelationId($id)
    {
        static::$correlationId = $id;
    }

    /**
     * Retrieve the correlation id.
     * Or gnerate a new unique id
     * @return string
     */
    public static function getCorrelationId()
    {
        $uuid = Uuid::v4();

        if (!static::$correlationId) {
            static::$correlationId = $uuid;
        }

        return  static::$correlationId;
    }
}
