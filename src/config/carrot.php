<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default carrot configuration
    |--------------------------------------------------------------------------
    | host
    | port
    | exchange
    */
    'config' => [
        'host' => env('CARROT_HOST', 'localhost'),
        'port' => env('CARROT_PORT', 5672),
        'exchange' => env('CARROT_EXCHANGE', 'ampq'),
        'logging' => env('CARROT_LOGGING', false),
        'localHost' => env('CARROT_LOCALHOST', url('/')),
        'instance' => env('CARROT_INSTANCE', 'Instance'),
        'module' => env('CARROT_MODULE', 'Module'),
        'appVersion' => env('CARROT_APP_VERSION', '1.0.0')
    ]
];
